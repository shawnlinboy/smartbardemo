package com.meizu.smartbar;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;

public class NoSmartBarActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_no_smart_bar);
		ActionBar mActionBar = getActionBar();
		mActionBar.show();
		
		/**
		 * 方法一：注意requestWindowFeature()必须在setContentView()之前
		 * 缺点是无法使用ActionBar
		 */
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		//SmartBarUtils.hide(this, getWindow());
		
		/**
		 * 方法二：此方法同样无法使用ActionBar
		 */
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //SmartBarUtils.hide(getWindow().getDecorView());
		
		/**
		 * 方法三：墙裂推荐使用此法，自由自在，想动就动
		 */
		SmartBarUtils.hide(getApplicationContext(), getWindow(),
		SmartBarUtils.SMART_BAR_HEIGH);
	}

}
