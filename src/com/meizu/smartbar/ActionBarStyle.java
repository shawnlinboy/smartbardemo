
package com.meizu.smartbar;

import android.app.ActionBar;
import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ActionBarStyle extends Activity {
	
	/**
	 * 自定义ActionBar的颜色与SpilitActionBar颜色
	 * 方法是重写style文件并在AndroidManifest.xml中设置该Activity的Theme
	 */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text_content);
        
        final ActionBar bar = getActionBar();
        
        /* 自定义Activity的返回键图标 */
        SmartBarUtils.setBackIcon(bar, getResources().getDrawable(R.drawable.ic_back));
        
        /* 自定义ActionBar Title的颜色*/
        setActionBarTitleColor();
    }

    /**
     * 设置ActionBar Title的颜色
     */
    private void setActionBarTitleColor() {
		// TODO Auto-generated method stub
    	int titleId = Resources.getSystem().getIdentifier("action_bar_title",
				"id", "android");
		TextView mTextView = (TextView) findViewById(titleId);
		mTextView.setTextColor(this.getResources().getColor(android.R.color.white));
	}


	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_layout_menu, menu);
        init(menu);
        return true;
    }

    
    private void init(Menu menu) {

        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            View actionView = item.getActionView();
            if (actionView != null) {
                // 监听actionview点击事件
                actionView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ActionBarStyle.this, "click", Toast.LENGTH_SHORT).show();
                    }
                });

                if (item.getGroupId() == R.id.tab_group) {
                    ImageView icon = (ImageView) actionView.findViewById(R.id.action_menu_icon);
                    TextView title = (TextView) actionView.findViewById(R.id.action_menu_title);
                    switch (item.getItemId()) {
                        case R.id.tab_menu_recent:
                            icon.setImageResource(R.drawable.ic_tab_recent);
                            title.setText("recent");
                            break;
                        case R.id.tab_menu_contacts:
                            icon.setImageResource(R.drawable.ic_tab_contacts);
                            title.setText("contacts");
                            break;
                        case R.id.tab_menu_dialer:
                            icon.setImageResource(R.drawable.ic_tab_dialer);
                            title.setText("dialer");
                            break;

                        default:
                            break;
                    }
                }
            }

        }
    }

}
